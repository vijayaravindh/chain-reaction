Chain Reaction
===================

Chain Reaction game implemented in three.js and bundled using webpack.

LINK: https://chain-reaction-3j.netlify.app/

## Preview
![image](chain-reaction.gif)

## Usage
After cloning install all node dependencies
```bash
npm i
```

Then launch the main task to open the livereload server  
```bash
npm start
```

## Deployment
```bash
npm run build
```
