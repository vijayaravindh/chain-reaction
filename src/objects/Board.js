import { Mesh, MeshLambertMaterial, BoxGeometry} from 'three'
import { preloader } from '../loader'

export default class extends Mesh {
  constructor (squareSize) {
    super()
    
    const boardTexture = preloader.get('board-texture')
  
    // create the board squares
    this.geometry = new BoxGeometry(squareSize-1, squareSize-1, squareSize-1);
    this.material =  new MeshLambertMaterial( { transparent: true,
                                                opacity : 0.2,
                                                map: boardTexture
                                              });
  
  }
}