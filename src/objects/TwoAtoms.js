import { Object3D, NearestFilter, MeshStandardMaterial} from 'three'
import { preloader } from '../loader'

export default class extends Object3D {
  constructor () {
    super()
    const atoms = preloader.get('atoms-2')
    this.material = new MeshStandardMaterial( { roughness: 0.2 , metalness: 0.7} );
    this.geometry = atoms.scene.children[0].geometry
  }
}