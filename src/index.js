import { WebGLRenderer, Scene, PerspectiveCamera, PointLight } from 'three'
import OrbitControls from './controls/OrbitControls'
import { preloader } from './loader'
import { TextureResolver } from './loader/resolvers/TextureResolver'
import { ImageResolver } from './loader/resolvers/ImageResolver'
import { GLTFResolver } from './loader/resolvers/GLTFResolver'
import { GameController } from "./game";
/* Custom settings */
// let stats

/* Init renderer and canvas */
const container = document.getElementById('game')
// const renderer = new WebGLRenderer()
// container.style.overflow = 'hidden'
// container.style.margin = 0
// container.appendChild(renderer.domElement)
// renderer.setClearColor(0x3d3b33)

/* Main scene and camera */
// const scene = new Scene()
// const camera = new PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000)
// const controls = new OrbitControls(camera)
// camera.position.z = 10
// controls.enableDamping = true
// controls.dampingFactor = 0.15
// controls.start()

// /* Lights */
// const frontLight = new PointLight(0xFFFFFF, 1)
// const backLight = new PointLight(0xFFFFFF, 1)
// scene.add(frontLight)
// scene.add(backLight)
// frontLight.position.set(20, 20, 20)
// backLight.position.set(-20, -20, 20)

/* Various event listeners */
// window.addEventListener('resize', onResize)

/* some stuff with gui */
if (DEVELOPMENT) {
  // const guigui = require('guigui')
  // guigui.add(SETTINGS, 'useComposer')

  const Stats = require('stats.js')
  var stats = new Stats()
  stats.showPanel(0)
  // container.appendChild(stats.domElement)
  stats.domElement.style.position = 'absolute'
  stats.domElement.style.top = 0
  stats.domElement.style.left = 0
}

let game = new GameController(container, stats);
/* Preloader */
preloader.init(new ImageResolver(), new GLTFResolver(), new TextureResolver())
preloader.load([
  { id: 'atoms-1', type: 'gltf', url: 'assets/models/atoms-1.glb' },
  { id: 'atoms-2', type: 'gltf', url: 'assets/models/atoms-2.glb' },
  { id: 'atoms-3', type: 'gltf', url: 'assets/models/atoms-3.glb' },
  { id: 'board-texture', type: 'texture', url: 'assets/textures/square_dark_texture.jpg' },
  { id: 'atoms-env-map', type: 'texture', url: 'assets/textures/board_texture.jpg' },
  { id: 'atoms-roughness-map', type: 'texture', url: 'assets/textures/board_texture.jpg' },

]).then(() => {
  // onResize()
  // animate()

  /* Actual content of the scene */
  game.drawField();
  game.onWindowResize();
  animate()
})




/**
  Resize canvas
*/
function onResize () {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
  renderer.setSize(window.innerWidth, window.innerHeight)
}

/**
  RAF
*/
// function animate() {
//   window.requestAnimationFrame(animate)
//   render()
// }

function animate() {
  console.log("Rendering new frame");
  game.renderObjects();
  requestAnimationFrame( animate );
}

/**
  Render loop
*/
function render () {
  if (DEVELOPMENT) {
    // stats.begin()
  }

  controls.update()
  renderer.clear()
  renderer.render(scene, camera)

  if (DEVELOPMENT) {
    // stats.end()
  }
}
