import { WebGLRenderer, Raycaster, MathUtils, Scene, AxesHelper, Mesh, PerspectiveCamera, PointLight, Vector2, AmbientLight, DirectionalLight } from 'three'
import OrbitControls from './controls/OrbitControls'
import * as dat from 'dat.gui';
import Board from "./objects/Board";
import OneAtom from "./objects/OneAtom";
import TwoAtoms from "./objects/TwoAtoms";
import ThreeAtoms from "./objects/ThreeAtoms";

export function GameController (containerElement, statsObj) {
    // var assetsUrl = assetsUrl;
    var containerElement = containerElement;
    var materials = {};
    var lights = {};
    var atomModels = [];
    var gameObjects = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ];
    var squareSize = 10;
    var rotationFactor = 0.2;
    var players = [0xff0000, 0x00ff00];
    var currentTurn = 0;
    var amidstTurn = 0;
    var popAudio = new Audio('assets/sprites/pop.mp3');
    var gameOver = false;
    var turns = 0;
    var renderer;
    var raycaster;
    var scene;
    var camera;
    var cameraController;
    var boardSquare;
    var stats = statsObj;

    const TWEEN = require('@tweenjs/tween.js')
    
    var resetGame = () => {
        for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 8; col++) {
                let currentObj = scene.getObjectByName("atom-"+row+"-"+col);
                if (typeof currentObj !== "undefined") {
                    currentObj.geometry.dispose();
                    currentObj.material.dispose();
                    scene.remove(currentObj);
                }
            }
        }
        initBoard();
        currentTurn = 0;
        amidstTurn = 0;
        gameOver = false;
        turns = 0;
        boardSquare.material.color.setHex(players[0])
    }

    this.drawField = () => {
        console.log("drawfield");
        initBoard();
        initEngine();
        initLights();
        initObjects();
        // initMaterials();
        initListeners();
        if (DEVELOPMENT) {
            initColorGui();
        }
        camera.updateProjectionMatrix()
    }
    
    const rotateObjects = () => {
        for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 8; col++) {
                var atom = gameObjects[row][col].model;
                var count = gameObjects[row][col].count;
                if (count > 1) {
                    // atom.rotation.x += rotationFactor * (count / gameObjects[row][col].criticalMass) ** 2;
                    atom.rotation.y += rotationFactor * ( count / gameObjects[row][col].criticalMass ) ** 2;
                    atom.rotation.z += rotationFactor * ( count / gameObjects[row][col].criticalMass ) ** 2;
                }
            }
        }
    }

    var checkForVictory = (player, successCallback) => {
        for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 8; col++) {
                if (gameObjects[row][col].player != null && 
                    player != gameObjects[row][col].player) {
                        return false;
                }
            }
        }
        successCallback(player);
    }

    this.renderObjects = () => {
        if (DEVELOPMENT) {
            stats.begin();
            }
        rotateObjects();
        renderer.render( scene, camera );
        TWEEN.update();
        if (DEVELOPMENT) {
            stats.end();
        }
        if (gameOver && TWEEN.getAll().length == 0) {
            setTimeout(resetGame(), 5000);
        }
    }

    const onWin = (player) => {
        if (!gameOver) {
            alert("Player " + (player+1) + " has won");
        }
        gameOver = true;
    }

    const initBoard = () => {
        for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 8; col++) {
                let criticalMass = 0;
                var nextLoc = [[0, 1], [0, -1], [1, 0], [-1, 0]];
                nextLoc.forEach(element => {
                    var nextRow = row + element[0];
                    var nextCol = col + element[1];
                    if (nextRow >= 0 && nextRow < 8 && nextCol >= 0 && nextCol < 8) {
                        criticalMass += 1;
                    }
                });
                gameObjects[row][col] = {  count : 0, 
                                                model : null,
                                                player: null,
                                                criticalMass: criticalMass
                                            }
            }
        }
        atomModels[0] = null;
    }
    /**
     * Initialize the scene, camera and the renderer
     */
    const initEngine = () => {
        console.log("initEngine");
        var viewWidth = containerElement.offsetWidth;
        var viewHeight = containerElement.offsetHeight;
        
        // instantiate the WebGL Renderer
        renderer = new WebGLRenderer({
            antialias: true
        });
        renderer.setSize(500, 800);
        renderer.setClearColor('#f0f0f0');
        
        // Create the raycaster
        raycaster = new Raycaster()

        // create the scene
        scene = new Scene();

        // Add Axes helper to get an idea of the coordinates
        if (DEVELOPMENT) {
            var axesHelper = new AxesHelper( 100 );
            scene.add(axesHelper);
        }

        // create the camera
        const aspectRatio = (viewWidth / viewHeight)
        let vFOV;
        if (aspectRatio >= 1) {
            vFOV = Math.atan( 90 / 240) * 2
        } else {
            vFOV = Math.atan( 90 / ( aspectRatio * 240) ) * 2
        }
        const fov = MathUtils.radToDeg(vFOV)
        camera = new PerspectiveCamera(fov, viewWidth / viewHeight, 1, 1000);
        camera.position.set( 40, 120, 40);
        camera.lookAt( 40, 0, 40);

        // Add a controller for the camera
        // cameraController = new OrbitControls(camera, renderer.domElement);
        // cameraController.start();
        // camera.rotation.set(0, 0, 0);      
        

        containerElement.appendChild(renderer.domElement);

        if (DEVELOPMENT) {
            containerElement.appendChild( stats.dom );
        }
    }
    
    /**
     * Initialize the lights.
     */
	const initLights = () => {
	    // top light
        lights.topLight = new PointLight();
        lights.topLight.position.set(40, 150, 40);
        lights.topLight.intensity = 1.0;
        
        // Ambient light
        lights.ambientLight = new AmbientLight('#ffffff', 1);

        
        // Diectional Lights
        lights.directionalLight1 = new DirectionalLight( 0xFFFFFF );
        lights.directionalLight1.position.set(0, 50, 0)
        lights.directionalLight1.target.position.set(40, 10, 40);
        lights.directionalLight1.castShadow =  true;

        lights.directionalLight2 = new DirectionalLight( 0xFFFFFF );
        lights.directionalLight2.position.set(80, 50, 0)
        lights.directionalLight2.target.position.set(40, 10, 40);
        lights.directionalLight2.castShadow =  true;

        lights.directionalLight3 = new DirectionalLight( 0xFFFFFF );
        lights.directionalLight3.position.set(0, 50, 80)
        lights.directionalLight3.target.position.set(40, 10, 40);
        lights.directionalLight3.castShadow =  true;

        lights.directionalLight4 = new DirectionalLight( 0xFFFFFF );
        lights.directionalLight4.position.set(80, 50, 80)
        lights.directionalLight4.target.position.set(40, 10, 40);
        lights.directionalLight4.castShadow =  true;

        // add the lights in the scene
        scene.add(lights.topLight);
        scene.add(lights.ambientLight);
        scene.add(lights.directionalLight1);
        scene.add(lights.directionalLight1.target);
        scene.add(lights.directionalLight2);
        scene.add(lights.directionalLight2.target);
        scene.add(lights.directionalLight3);
        scene.add(lights.directionalLight3.target);
        scene.add(lights.directionalLight4);
        scene.add(lights.directionalLight4.target);
	}

    /**
     * Initialize the objects.
    */
	const initObjects = () => {
        boardSquare = new Board(10);
	    for (var row = 0; row < 8; row++) {
            for (var col = 0; col < 8; col++) {
                let tile = boardSquare.clone();
                tile.position.x = col * squareSize + squareSize / 2;
                tile.position.z = row * squareSize + squareSize / 2;
                tile.position.y = 5; 
                tile.name = "board-" +row + '-' + col;                       
                scene.add(tile);
            }
        }
        boardSquare.material.color.setHex(players[0]);
        let atom = new OneAtom();
        atomModels.push(atom);
        atom = new TwoAtoms();
        atomModels.push(atom);
        atom = new ThreeAtoms();
        atomModels.push(atom);
    }
    
    /**
     * Initialize listeners.
     */
    const initListeners = () => {
        var domElement = renderer.domElement;     
        domElement.addEventListener('click', onMouseClick);
        window.addEventListener( 'resize', this.onWindowResize);
    }


    this.onWindowResize = (event) => {
        var width = containerElement.offsetWidth;
        var height = containerElement.offsetHeight;
        const aspectRatio = (width / height)
        let vFOV;
        if (aspectRatio >= 1) {
            vFOV = Math.atan( 90 / 240) * 2
        } else {
            vFOV = Math.atan( 90 / ( aspectRatio * 240) ) * 2
        }
        camera.fov = MathUtils.radToDeg(vFOV)
        camera.aspect = aspectRatio
		camera.updateProjectionMatrix();
        renderer.setSize( width, height );
    }

    /**
     * Mouse down event handler
    */
    const onMouseClick = (event) => {
        if (amidstTurn === 0 && TWEEN.getAll().length === 0)
        {
            let width = containerElement.offsetWidth;
            let height = containerElement.offsetHeight;
            var mouse = new Vector2();
            mouse.x = ( event.clientX / width ) * 2 - 1;
            mouse.y = - ( event.clientY / height ) * 2 + 1;
            raycaster.setFromCamera( mouse, camera );
            const intersects = raycaster.intersectObjects( scene.children );
            if ( intersects.length > 0 ) {
                var INTERSECTED;
                for (var i=0; i < intersects.length; i++) {
                    if (intersects[ i ].object.name.startsWith("board")) {
                        INTERSECTED = intersects[ i ].object;
                        break;
                    }
                }
                var boardX, boardZ;
                boardX = Math.floor((INTERSECTED.position.x - squareSize / 2)  / squareSize);
                boardZ = Math.floor((INTERSECTED.position.z - squareSize / 2)  / squareSize);
                if (gameObjects[boardZ][boardX].player === null || gameObjects[boardZ][boardX].player === currentTurn) {
                    addToBoard(boardZ, boardX, currentTurn);
                    currentTurn = (currentTurn + 1) % players.length;
                    turns += 1;
                    boardSquare.material.color.setHex(players[currentTurn])
                } else {
                    alert("Invalid Move!!!!!!");
                }
            }
        }
    }

    const addToBoard = (row, col, player, depth=0) => {
        amidstTurn += 1;
        console.log("Row: "+row+" Col:"+col+" Depth:"+depth);
        var currentObj = gameObjects[row][col];
        var count = currentObj.count;
        var x = squareSize * (col + 0.5);
        var y = 5;
        var z = squareSize * (row + 0.5);
        if (count != 0) {
            scene.remove(currentObj.model);
            currentObj.model.geometry.dispose();
            currentObj.model.material.dispose();
        }
        count =  (count + 1 ) % currentObj.criticalMass;
        gameObjects[row][col].count = count;
        gameObjects[row][col].player = player;
        if (count != 0) {
            var material = atomModels[count].material.clone();
            var model = new Mesh(atomModels[count].geometry, material);
            model.scale.setScalar(2);
            model.material.color.setHex(players[player]);
            model.position.set(x, y , z);
            model.name = "atom-"+row+"-"+col;
            scene.add(model)
            gameObjects[row][col].model = model;
        } else {
            gameObjects[row][col].model = null;
            gameObjects[row][col].player = null;
            var nextLoc = [[0, 1], [0, -1], [1, 0], [-1, 0]];
            popAudio.play();
            
            nextLoc.forEach(element => {
                var nextRow = row + element[0];
                var nextCol = col + element[1];
                if (nextRow >= 0 && nextRow < 8 && nextCol >= 0 && nextCol < 8) {
                    var model = new Mesh(atomModels[1].geometry, atomModels[1].material.clone());
                    model.scale.setScalar(2)
                    model.material.color.setHex(players[player]);
                    model.position.set(x, y, z);
                    model.name = "atom-"+row+"-"+col;
                    scene.add(model);
                    setupPositionTween(model, nextRow, nextCol, depth, player);
                }
            });
        }
        amidstTurn -= 1;
        if (turns > 2) {
            checkForVictory(player, onWin);
        }
    }

    const setupPositionTween = (object , targetRow, targetCol, depth, player) => {
        var target = {
            x : (targetCol * squareSize + squareSize / 2),
            y : 10,
            z : (targetRow * squareSize + squareSize / 2)
        };
        var easing = TWEEN.Easing.Exponential;
        var tween	= new TWEEN.Tween(object.position)
        .to(target, 500)
        .onComplete(() => {
            scene.remove(object);
            addToBoard(targetRow, targetCol, player, depth+1);
        });
        tween.start();
    }

    const initColorGui = () => {
        const makeXYZGUI = (gui, vector3, range, name, onChangeFn) => {
            const folder = gui.addFolder(name);
            folder.add(vector3, 'x', -range, range).onChange(onChangeFn);
            folder.add(vector3, 'y', -range, range).onChange(onChangeFn);
            folder.add(vector3, 'z', -range, range).onChange(onChangeFn);
            folder.open();
        }
        function updateCamera() {
            camera.updateProjectionMatrix();
        }
        const gui = new dat.GUI();
        makeXYZGUI(gui, camera.position, 200, "camera", updateCamera);
        makeXYZGUI(gui, camera.rotation, 2, "cameraRotation", updateCamera)
    }

}

//https://github.com/divija96